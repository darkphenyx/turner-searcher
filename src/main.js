import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'sweetalert2/dist/sweetalert2.min.css';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.use(VueSweetalert2);

new Vue({
  render: h => h(App),
}).$mount('#app');
