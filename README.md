# Turner Title Search

This project was built using Electron.js, Vue, and MongoDB. Just search for the title and see if the test DB has it in there.

## Project setup
```
yarn #
```

### Compiles and hot-reloads for development
```
yarn run electron:serve
```

### Compiles and minifies for production
```
yarn run electron:build
```

### Lints and fixes files
```
yarn run lint
```
